package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        int yi = 0;
        int xi = 0;

        while (yi < y.size() && xi < x.size()) {
            Object elY = y.get(yi);
            Object elX = x.get(xi);

            if (Objects.equals(elY, elX)) {
                yi++;
                xi++;
            } else {
                yi++;
            }
        }
        return xi == x.size();
    }
}
