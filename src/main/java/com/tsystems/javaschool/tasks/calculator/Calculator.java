package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        }
        if (!statement.matches("[0-9a-zA-Z-+/*\\s=().]+")) {
            return null;
        }

        StringBuilder operand = new StringBuilder();
        StringBuilder operator = new StringBuilder();

        Deque<Character> stack = new ArrayDeque<>();
        StringBuilder postfix = new StringBuilder();

        try {
            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if (Character.isWhitespace(c)) {
                    continue;
                }

                if (Character.isLetterOrDigit(c) || c == '.') {
                    if (operator.length() != 0) {
                        if (operator.toString().equals("-") && statement.charAt(i - 1) == '-' && statement.charAt(i - 2) == ' ') {
                            operand.append('-');
                        } else {
                            parseOperator(operator, stack, postfix);
                        }
                        operator.setLength(0);
                    }
                    operand.append(c);
                } else {
                    if (operand.length() != 0) {
                        postfix.append(operand).append(" ");
                        operand.setLength(0);
                    }
                    if (operator.length() != 0) {
                        parseOperator(operator, stack, postfix);
                        operator.setLength(0);
                    }
                    operator.append(c);
                }
            }
            if (operand.length() != 0) {
                postfix.append(operand).append(" ");
            }
            if (operator.length() != 0) {
                parseOperator(operator, stack, postfix);
            }

            while (!stack.isEmpty()) {
                char o = stack.pop();
                if (o == '(' || o == ')') {
                    throw new IllegalArgumentException("Invalid expression");
                }
                postfix.append(o).append(" ");
            }
            postfix.deleteCharAt(postfix.length() - 1);

            return calculate(postfix.toString().trim());
        } catch (Exception e) {
            return null;
        }
    }

    private static void parseOperator(CharSequence s, Deque<Character> stack, StringBuilder sb) {
        char c;
        if (s.length() != 1) {
            c = parseSign(s);
        } else {
            c = s.charAt(0);
        }

        if (c == ')') {
            char i;
            while ((i = stack.pop()) != '(') {
                sb.append(i).append(" ");
            }
        } else if (stack.isEmpty() ||
                stack.peek() == '(' ||
                getPrecedence(c) > getPrecedence(stack.peek()) ||
                c == '(') {
            stack.push(c);
        } else if (getPrecedence(c) <= getPrecedence(stack.peek())) {
            while (!stack.isEmpty() &&
                    stack.peek() != '(' &&
                    getPrecedence(stack.peek()) >= getPrecedence(c)) {
                sb.append(stack.pop()).append(" ");
            }
            stack.push(c);
        }
    }

    private static char parseSign(CharSequence s) {
        boolean plus = true;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '-') {
                plus = !plus;
            }
        }
        return plus ? '+' : '-';
    }

    private static int getPrecedence(char c) {
        switch (c) {
            case '-':
            case '+':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }

    private static String calculate(String postfix) {
        Deque<Double> stack = new ArrayDeque<>();

        for (String s : postfix.split("\\s+")) {
            if (s.matches("-?[0-9]+(\\.[0-9]+)?")) {
                stack.push(Double.parseDouble(s));
            } else {
                double y = stack.pop();
                double x = stack.pop();
                double r = performOperation(s, x, y);
                stack.push(r);
            }
        }
        double res = stack.pop();
        if (!stack.isEmpty()) {
            return null;
        } else {
            if (Double.isInfinite(res)) {
                return null;
            }
            return res == (int) res ? (int) res + "" : res + "";
        }
    }

    private static double performOperation(String operator, double x, double y) {
        switch (operator) {
            case "+":
                return x + y;
            case "-":
                return x - y;
            case "*":
                return x * y;
            case "/":
                return x / y;
            default:
                return -1;
        }
    }
}
