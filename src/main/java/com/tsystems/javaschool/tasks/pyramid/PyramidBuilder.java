package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int height = calcHeight(inputNumbers);
        if (inputNumbers.contains(null) || height == -1) {
            throw new CannotBuildPyramidException();
        }

        int[][] pyramid = new int[height][height * 2 - 1];

        Collections.sort(inputNumbers);
        Iterator<Integer> iterator = inputNumbers.iterator();

        for (int j = height - 1, i = 0; i < pyramid.length; i++, j--) {
            for (int k = j; k < pyramid[i].length - j; k += 2) {
                pyramid[i][k] = iterator.next();
            }
        }

        return pyramid;
    }

    private int calcHeight(List<Integer> list) {
        for (int i = 1, j = 2; i <= list.size() && i > 0; i += j, j++) {
            if (list.size() == i) {
                return j - 1;
            }
        }
        return -1;
    }


}
